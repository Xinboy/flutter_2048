import 'package:flutter/material.dart';
import 'package:flutter_2048/constants/game_2048_colors.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_2048/widgets/game_2048_panel.dart';

class Game2048Page extends StatefulWidget {
  @override
  _Game2048PageState createState() => _Game2048PageState();
}

class _Game2048PageState extends State<Game2048Page> {
  /// 历史最高分，本地存储key
  static const game2048HighestScoreKey = "game2048HighestScoreKey";

  /// 当前分数
  int currentScore = 0;

  /// 历史最高分
  int highestScore = 0;

  final Future<SharedPreferences> _spFuture = SharedPreferences.getInstance();

  /// 用于获取 Game2048PanelState 实例，以便可以调用restartGame方法
  GlobalKey _gamePanelKey = GlobalKey<Game2048PanelState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
//    readHighestScoreFromSp();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      body: Container(
        padding: const EdgeInsets.only(top: 30),
        color: Game2048Colors.bgColor1,
        child: OrientationBuilder(
          builder: (context, orientation) {
            if (orientation == Orientation.portrait) {
              return Column(
                children: [
                  Flexible(child: buildHeaderContainer()),
                  Flexible(flex: 2, child: buildGamePanel()),
                ],
              );
            } else {
              return Row(
                children: [
                  Flexible(child: buildHeaderContainer()),
                  Flexible(flex: 2, child: buildGamePanel()),
                ],
              );
            }
          },
        ),
      ),
    );
  }

  Widget buildHeaderContainer() {
    return Container(
      padding: const EdgeInsets.all(20),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "2048",
                  style: TextStyle(
                      color: Game2048Colors.textColor1,
                      fontSize: 56,
                      fontWeight: FontWeight.bold),
                ),
                Text(
                  "Play 2048 Game Now",
                  style: TextStyle(
                      color: Game2048Colors.textColor1,
                      fontSize: 14,
                      fontWeight: FontWeight.w500),
                ),
                const SizedBox(
                  height: 2,
                ),
                Text(
                  "Join numbers and get to the 2048 tile!",
                  style: TextStyle(color: Game2048Colors.textColor1),
                ),
              ],
            ),
          ),
          const SizedBox(
            width: 10,
          ),
          SizedBox(
            width: 100,
            child: Column(
              children: [
                Container(
                  padding: const EdgeInsets.all(12),
                  width: double.infinity,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    color: Game2048Colors.bgColor2,
                  ),
                  child: Column(
                    children: [
                      Text(
                        "SCORE",
                        style: TextStyle(
                          color: Game2048Colors.textColor2,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      const SizedBox(
                        height: 2,
                      ),
                      Text(
                        currentScore.toString(),
                        style: TextStyle(
                          color: Game2048Colors.textColor2,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Container(
                  padding: const EdgeInsets.all(12),
                  width: double.infinity,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    color: Game2048Colors.bgColor2,
                  ),
                  child: Column(
                    children: [
                      Text(
                        "HIGHEST",
                        style: TextStyle(
                          color: Game2048Colors.textColor2,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      const SizedBox(
                        height: 2,
                      ),
                      Text(
                        highestScore.toString(),
                        style: TextStyle(
                            color: Game2048Colors.textColor3,
                            fontWeight: FontWeight.bold,
                            fontSize: 20),
                      )
                    ],
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Container(
                  width: double.infinity,
                  padding: const EdgeInsets.all(12),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    color: Game2048Colors.bgColor3,
                  ),
                  child: Center(
                    child: InkWell(
                      onTap: () {
                          (_gamePanelKey.currentState as Game2048PanelState).restartGame();
                      },
                      child: const Text(
                        "NEW GAME",
                        style: TextStyle(
                            fontSize: 11,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget buildGamePanel() {
    return Game2048Panel(
        key: _gamePanelKey,
        onScoreChanged: (score) {
          setState(() {
            currentScore = score;
            if (currentScore > highestScore) {
              highestScore = currentScore;
              storeHighestScoreToSp();
            }
          });
        });
  }

  /// 读取历史最高分
  readHighestScoreFromSp() async {
    final SharedPreferences sp = await _spFuture;
    setState(() {
      highestScore = sp.getInt(game2048HighestScoreKey) ?? 0;
    });
  }

  /// 存储历史最高分
  storeHighestScoreToSp() async {
    final SharedPreferences sp = await _spFuture;
    await sp.setInt(game2048HighestScoreKey, highestScore);
  }
}
